defmodule NucleotideCount do
  @nucleotides [?A, ?C, ?G, ?T]

  @doc """
  Counts individual nucleotides in a DNA strand.

  ## Examples

  iex> NucleotideCount.count('AATAA', ?A)
  4

  iex> NucleotideCount.count('AATAA', ?T)
  1
  """
  @spec count(charlist(), char()) :: non_neg_integer()
  def count(strand, nucleotide) do
    count(strand, nucleotide, 0)
  end

  defp count([nucleotide | remaining], nucleotide, num) do
    count(remaining, nucleotide, num + 1)
  end

  defp count([_ | remaining], nucleotide, num) do
    count(remaining, nucleotide, num)
  end

  defp count([], _, num) do
    num
  end

  @doc """
  Returns a summary of counts by nucleotide.

  ## Examples

  iex> NucleotideCount.histogram('AATAA')
  %{?A => 4, ?T => 1, ?C => 0, ?G => 0}
  """
  @spec histogram(charlist()) :: map()
  def histogram(strand) do
    @nucleotides
      |> Enum.map(fn val -> {val, count(strand, val)} end)
      |> Map.new
  end
end
